# Rigs offline userscript #

This userscript tells you when a rig goes offline and when a rig has a lower hashrate than usual using data from the Ninjapool workers page.

To make it work put your account's address and your rigs data (use the example in the JSON file) in the settings at the top of the userscript.js file and install it in your browser.