// ==UserScript==
// @name         Rigs down
// @version      0.1
// @match        http://ethclassic.ninjapool.jp/*
// ==/UserScript==

//Settings

//Your address
var address = "";

//JSON with rigs data
var rigs = jQuery.parseJSON('');

//An hashrate lower than this number is considered low
var hashlimit = 105;

//Hashrate to check for rigs with low hashrate
//1 rough, short average
//2 accurate, long average
var hashtype = 2;

//Update interval in milliseconds
var interval = 5000;

//End settings

var originaltitle = document.title;

//Add jQuery, unless it already exists
if(typeof jQuery === 'undefined'|| !jQuery){
    (function(){
        var s=document.createElement('script');
        s.setAttribute('src','http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js');
        if(typeof jQuery=='undefined'){
            document.getElementsByTagName('head')[0].appendChild(s);
        }
    })();
}

function update() {
    var rigsup = [];
    var lowhasharr = [];
    $("#rigsdown").appendTo($("#ember307").children(".container:first"));
    $('#rigsdown').show();
    $("#lowhash").appendTo($("#ember307").children(".container:first"));
    $('#lowhash').show();

    //Get worker id of all rigs that are up
    $('.success').each(function() {
        rigsup.push($(this).children().first().text());
    });

    //Get worker id and hashrate of workers with low hashrate
    $('.success').each(function() {
        hashtext = $(this).children().eq(hashtype).text();
        hash = hashtext.substring(0, hashtext.length - 6);
        if (parseInt(hash) < hashlimit) {
            lowhasharr[$(this).children().first().text()] = hashtext;
        }
    });

    //Get worker id of all rigs that are down comparing all rigs and rigs that are up
    rigsdown = $.grep(allrigs, function(x) {
        return $.inArray(x, rigsup) < 0;
    });

    if (typeof rigsdown !== 'undefined' && rigsdown.length > 0) {
        var table = "<table class='table table-condensed table-striped'><tr><th>ID</th><th>Location</th><th>IP</th><th>MAC address</th></tr>";
        $.each(rigs, function(i, item) {
            if (jQuery.inArray(item.workerid, rigsdown) != -1) {
                var name = item.name;
                table += "<tr style='background-color: #f0d8d8'><td>" + item.workerid + "</td><td>" + name + "</td><td>" + item.ip  + "</td><td>" + item.mac + "</td></tr>";
                return;
            }
        });
        table += "</table>";
        $('#rigsdown').html("<h4>Workers Down</h4>" + table);
        if (rigsdown.length == 1) {
            document.title = "1 rig down - " + originaltitle;
        } else {
            document.title = rigsdown.length + " rigs down - " + originaltitle;
        }
    } else {
        document.title = originaltitle;
        $('#rigsdown').hide();
    }

    var size = Object.keys(lowhasharr).length;

    if (size > 0) {
        var table2 = "<table class='table table-condensed table-striped'><tr><th>ID</th><th>Location</th><th>IP</th><th>MAC address</th><th>Hashrate</th></tr>";
        $.each(rigs, function(i, item) {
            if (item.workerid in lowhasharr) {
                var name = item.name;
                table2 += "<tr><td>" + item.workerid + "</td><td>" + name + "</td><td>" + item.ip  + "</td><td>" + item.mac + "</td><td>" + lowhasharr[item.workerid] + "</td></tr>";
                return;
            }
        });
        table2 += "</table>";
        $('#lowhash').html("<h4>Workers with low Hashrate</h4>" + table2);
    } else {
        $('#lowhash').hide();
    }
}

var allrigs = [];

$.each(rigs, function() {
    allrigs.push(this.workerid);
});

(function(){
    var codeToExecute = function(){
        if (window.location.href.indexOf(address) > -1) {
            $('body').append('<div id="rigsdown"></div>');
            $('#rigsdown').hide();
            $('body').append('<div id="lowhash"></div>');
            $('#lowhash').hide();
            window.setInterval(function(){
                if (window.location.href.indexOf("payouts") == -1) {
                    update();
                } else {
                    $('#rigsdown').hide();
                    $('#lowhash').hide();
                }
            }, interval);
        }
    };

    var intervalInt = window.setInterval(function(){
        if(typeof jQuery !== 'undefined' && jQuery){
            // Clear this interval
            window.clearInterval(intervalInt);
            codeToExecute();
        }
    }, 100);
})();
